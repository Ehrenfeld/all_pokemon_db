Pour charger la DB Pokémon : 

    1 - Run "Pokémons.sql" dans votre bdd. 
    2 - Enregister. 

Inclut : 

    - Nom Français (anglais).
    - N° pokédex.
    - Sprite des Pokémon. 
    - Type 1 et Type 2.
    - Méga-Évolution.
    - Male/femelle.
    - Forme alternative.
    - Forme d'Alola.

ENJOY ! ;)